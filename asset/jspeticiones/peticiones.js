$(document).ready(function () {
    
    jQuery.ajaxSetup({
        beforeSend: function() {
           $('#loadingDiv').show();
        },
        complete: function(){
           $('#loadingDiv').hide();
        },
        success: function() {}
      });

    function mostrarCartellera(res) {
        //console.log(res);

        let numPeliculas = res.records.length;
        let infopelicula = "";
        for (let i = 0; i < numPeliculas; i++) {
            //generes a array//
            generesArray = res.records[i].generes.split("#");

            infopelicula += '<div id="'+res.records[i].id+'" class="infopelicula col-lg-12">';
            infopelicula += '<img class="col-lg-2" src="' + res.records[i].poster_path + '" width="150px" alt="Portada de la pelicula" />';
            infopelicula += '<div class="box_info_pelicula_cartellera"><div class="descripciopelicula col-lg-5">';
            infopelicula += '<span class="generes">';

            for(cont=1;cont < generesArray.length;cont++){
                infopelicula +='<span class="gen">'+generesArray[cont]+'</span>';
            }

            infopelicula +='</span>';
            infopelicula += '<p><strong>Titol: </strong>'+ res.records[i].titol +'</p>';
            infopelicula += '<p><strong>Durada: </strong>' + res.records[i].duracio +' min</p>';
            infopelicula += '<p><strong>Descripció: </strong>'+ res.records[i].descripcio +'</p>';
            infopelicula += '</div>';
            infopelicula +='<div class="degradat_video"></div>';
            infopelicula += '<iframe class="col-lg-5" title="Trailer" src="https://www.youtube.com/embed/' + res.records[i].url_video + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            infopelicula += '</div></div>';
        }
        document.getElementById("infoTodasPeliculas").innerHTML = infopelicula;
    }

    function mostrarCarrosels(res,id_div) {
        let numPeliculas = res.records.length;
        let infocarrosel = "";
        let infoalone = "";
        for (let i = 0; i < numPeliculas; i++) {
            if(res.records[i].estat==null){
                //console.log(res);
                if(res.records.length<2){
                    //console.log("Una caratula");
                    infoalone += '<img src="/asset/caratules/' + res.records[i].url + '" alt="Informaccio i sessions de la pelicules en cartellera"/>';
                    document.getElementById("caratula_alone").innerHTML = infoalone;

                    $(".cartellera").hide();

                }else{
                //console.log(res);
                infocarrosel += '<div class="item">';
                infocarrosel += '<div class="partner-logo"><img  src="/asset/caratules/' + res.records[i].url + '" alt="Informaccio i sessions de la pelicules en cartellera"/></a></div>';
                infocarrosel += '</div>';
                }
                
     
            }else if(res.records[i].estat==1){
                if(window.numeroestrenes>1){
                    infocarrosel += '<div class="item">';
                    infocarrosel += '<div class="partner-logo-estrenes"><a><img  idPelicula="'+i+'" src="' + res.records[i].poster_path + '" alt="Proximes pelicules carrousel"></a></div>';
                    infocarrosel += '</div>';
                }else{
                    $("#carroselEstrenes").hide();
                }
                
            }
            
        }
        document.getElementById(""+id_div).innerHTML = infocarrosel;
    }
    
    function mostrarEstrena(res,id_p=0) {
        let infopelicula = "";
        if(res.records[0]!=undefined){
            
            generesArray = res.records[id_p].generes.split("#");

            infopelicula += '<div id="'+res.records[id_p].id+'" class="infopelicula box_info_pelicula_estrena col-lg-12" >';
            infopelicula += '<img class="col-lg-2" src="' + res.records[id_p].poster_path + '" width="150px" alt="Portada de la pelicula en estrenes"/>';
            infopelicula += '<div class="descripciopelicula col-lg-5">';
            infopelicula += '<span class="generes">';

            for(cont=1;cont < generesArray.length;cont++){
                infopelicula +='<span class="gen">'+generesArray[cont]+'</span>';
            }

            infopelicula +='</span>';
            infopelicula += '<p><strong>Titol: </strong>'+ res.records[id_p].titol +'</p>';
            infopelicula += '<p><strong>Durada: </strong>' + res.records[id_p].duracio +' min</p>';
            infopelicula += '<p><strong>Descripció: </strong>'+ res.records[id_p].descripcio +'</p>';
            infopelicula += '</div>';
            infopelicula +='<div class="degradat_video"></div>';
            infopelicula += '<iframe class="col-lg-5" src="https://www.youtube.com/embed/' + res.records[id_p].url_video + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            infopelicula += '</div>';
        document.getElementById("informacioEstrenes").innerHTML = infopelicula;
        }else{
            infopelicula += '<div class="col-lg-12 text-center" >';
            infopelicula += "<p>No hi ha proximes estrenes</p>";
            infopelicula += '</div>';
            document.getElementById("informacioEstrenes").innerHTML = infopelicula;
        }
        
    }

    function recuperar_caratules_principals(){
        $.ajax({
            method: "GET",
            url: direccio_servidor + "/admin/api.php/records/caratules",
        })
            .done(function (caratules) {
                //console.log(caratules);
                let id_div = "carroselCartellera";
                mostrarCarrosels(caratules, id_div);
        $('#carroselCartellera').owlCarousel({
            loop:true,
            nav:false,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:1
                },
                1000:{
                    items:2
                }
            }
        })

    });
    }

    function recuperarInforPelicula() {
        $.ajax({
            method: "GET",
            url: direccio_servidor + "/admin/api.php/records/pelicules?filter=estat,eq,2&order=id,desc",
        })
            .done(function (res) {
                //console.log(res);
                mostrarCartellera(res);
        });
    }

    function recuperarEstrenes() {
        $.ajax({
            method: "GET",
            url: direccio_servidor + "/admin/api.php/records/pelicules?filter=estat,eq,1&order=id,desc",
        })
            .done(function (res) {
                window.numeroestrenes = res.records.length;
                let id_div = "carroselEstrenes";
                let seleccionat_anterior ="";
                mostrarCarrosels(res, id_div);
                mostrarEstrena(res);
                $("#carroselEstrenes").on('click mouseover', 'img',function(){
                    let idPelicula =  $(this).attr("idPelicula");
     
                    if(idPelicula != seleccionat_anterior){
                        seleccionat_anterior = idPelicula;
                        mostrarEstrena(res,idPelicula);
                    }

                  })
                $('#carroselEstrenes').owlCarousel({
                    loop:true,
                    margin:10,
                    nav:false,
                    autoplay:true,
                    autoplayTimeout:1500,
                    autoplayHoverPause:true,
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:2
                        },
                        1000:{
                            items:4
                        }
                    }
                })

            });
    }
    recuperarEstrenes();
    recuperarInforPelicula();
    recuperar_caratules_principals();
    
});