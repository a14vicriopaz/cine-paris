
$(document).ready(function () {

    actualitzar_biblioteca();

    // Recuperme les pelicules de la biblioteca //
    function actualitzar_biblioteca(){
        $.ajax({
            method: "GET",
            url: direccio_servidor+"/admin/api.php/records/pelicules?order=id,desc",
          })
          .done(function(res) {
            //console.log(res);
            let n_pelicules_a_biblioteca = res.records.length;
            let html = "";

            for(n=0;n<n_pelicules_a_biblioteca;n++){

                id_pelicula = res.records[n].id;
                titol = res.records[n].titol;
                url = res.records[n].poster_path;
                estat = res.records[n].estat;
                
                html += "<div class='pelicula pelicula_biblioteca' attr='"+url+"' id='peli_"+id_pelicula+"'>";
                html += "<button class='button-error b_eliminar_peli'><i class='but_eliminar fa fa-times' aria-hidden='true'></i></button>";
               
                if(estat==0){
                    html += "<div class='estat_pelicula estat_nou'><i class='far fa-eye-slash'></i></div>";
                }else if(estat == 1){
                    html += "<div class='estat_pelicula estat_prox'><i class='fas fa-calendar-alt'></i></div>";
                }else if(estat == 2){
                    html += "<div class='estat_pelicula estat_pub'><i class='far fa-eye'></i></div>";
                }
                
                html += "<img src='"+url+"' class='caratules'/>";
                html += "<div class='mod_estat_pelicula'>";
                html += "<button class='button-success b_mod_estat' attr='2'>Cartellera</button>";
                html += "<button class='button-warning b_mod_estat' attr='1'>Pròximes</button>";
                html += "</div>";
                html += "<div class='info_pelicula'>";
                html += "<p class='titol_pelicula'>"+titol+"</p>";
                html += "</div>";
                html += "</div>";

                assignar_estats(estat,id_pelicula);
            }
            document.getElementById("biblioteca_pelicules").innerHTML = html;
            
            // Mostrar les pelicules que estan a la biblioteca //
            function update_estats(estat_pelicula,seleccionat){
                $.ajax({
                    method: "PUT",
                    url: direccio_servidor+"/admin/api.php/records/pelicules/"+seleccionat,
                    data:{
                        estat:estat_pelicula
                    }
                  })
                  .done(function(res) {
                    //console.log(res);
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Estat Actualitzat',
                        showConfirmButton: false,
                        timer: 1000
                      })
                  });
            }

            function assignar_estats(estat,seleccionat){
        
                if(estat == 1){
                    //style//
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_nou" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_pub" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").addClass( "estat_prox" );
                    //content//
                    $("#peli_"+seleccionat).children(".estat_pelicula").empty();
                    $("#peli_"+seleccionat).children(".estat_pelicula").append("<i class='far fa-calendar-alt'></i>");

                    
                }else if(estat == 2){
                    //style//
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_nou" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_prox" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").addClass( "estat_pub" );
                    //content//
                    $("#peli_"+seleccionat).children(".estat_pelicula").empty();
                    $("#peli_"+seleccionat).children(".estat_pelicula").append("<i class='far fa-eye'></i>");

                }else if(estat == 0){
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_pub" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").removeClass( "estat_prox" );
                    $("#peli_"+seleccionat).children(".estat_pelicula").addClass( "estat_nou" );
                    //content//
                    $("#peli_"+seleccionat).children(".estat_pelicula").empty();
                    $("#peli_"+seleccionat).children(".estat_pelicula").append("<i class='far fa-eye-slash'></i>");
                }
            }

            // Posar estat a 0 //
            $("#biblioteca_pelicules").on("click", ".estat_pelicula", function () {
                estat = 0;
                seleccionat = $(this).parent().attr('id').substring(5);
                update_estats(estat,seleccionat);
                assignar_estats(estat,seleccionat);
            });

            // Cambiar estat de la pelicula //
            $("#biblioteca_pelicules").on("click", ".b_mod_estat", function () {
                estat = $(this).attr('attr');
                seleccionat = $(this).parent().parent().attr('id').substring(5);
                update_estats(estat,seleccionat);
                assignar_estats(estat,seleccionat);
            });
        })
    }

    function borrar_pelicula(seleccionat){
        seleccionat = seleccionat.substring(5);

        $.ajax({
            method: "DELETE",
            url: direccio_servidor+"/admin/api.php/records/pelicules/"+seleccionat,
          })
          .done(function() {
            console.log(seleccionat+" borrada");
            actualitzar_biblioteca();
          })
    }

    function borrar_poster_al_server(path_to_file,seleccionat) {

        $.ajax({
          url: 'remover.php',
          data: { 'caratula': path_to_file },
          type: 'post',
          success: function () {
            borrar_pelicula(seleccionat);
          },
          error: function (error) {
            console.log("Error al borrar l'arxiu " + error);
          }
        });
      }

    $("#biblioteca_pelicules").on("click", ".b_eliminar_peli", function () {
        seleccionat = $(this).parent().attr('id');
        img_a_borrar = $(this).parent().attr('attr').split('%20').join(' ');
        console.log(img_a_borrar);
        borrar_poster_al_server(img_a_borrar,seleccionat);
        
        
    });
});