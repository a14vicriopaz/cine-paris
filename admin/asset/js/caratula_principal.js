$(document).ready(function () {

  function guardar_bd_caratula(url_caratula) {
    $.ajax({
      method: "POST",
      url: direccio_servidor + "/admin/api.php/records/caratules",
      data: {
        url: url_caratula,
      }
    })
      .done(function () {
        //alert("Caratula afegida correctament");
        location.reload();
      })
  }

  function pujar_caratula_al_server(file_data, form_data) {
    console.log(file_data);

    if(file_data !== undefined){
      $.ajax({
        url: 'uploader_caratula.php', 
        dataType: 'text', 
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function () {
          url_caratula = file_data.name.split(' ').join('%20');
          console.log(url_caratula);
          guardar_bd_caratula(url_caratula);
        },
        error: function () {
          Swal.fire({
            position: 'center',
            type: 'error',
            title: "error al pujar l'imatge",
            showConfirmButton: false,
            timer: 1500
          })
        }
      });
    }else{
      Swal.fire({
        position: 'center',
        type: 'error',
        title: "No s'ha seleccionat cap imatge",
        showConfirmButton: false,
        timer: 1500
      })
    }
  }

  // Guardar a la bd la caratula i pujar al servidor
  $('#pujar_file').on('click', function () {
    $("#seleccionar_file").show();
    $("#pujar_file").hide();
  });
  
  $("#pujar_file").hide();

  $('#seleccionar_file').on('click', function () {
    $("#seleccionar_file").hide();
    $("#pujar_file").show();

    $('#pujar_file').on('click', function () {
      let file_data = $('#sortpicture').prop('files')[0];
      let form_data = new FormData();
      form_data.append('file', file_data);

      pujar_caratula_al_server(file_data, form_data);
    });

  });

  // Mostrar les caratules //
  function mostrar_caratules(caratula) {
    //console.log(caratula);
    let n_caratules = caratula.records.length;

    let caratules = "";
    for (n = 0; n < n_caratules; n++) {
      id_caratula = caratula.records[n].id;
      ulr_caratula = caratula.records[n].url;
      url_caratula_p = "../asset/caratules/" + caratula.records[n].url;

      //console.log(url_caratula_p);

      caratules += "<div class='box_caratula pelicula' id='caratula_" + id_caratula + "' attr='" + ulr_caratula + "'>";
      caratules += "<button class='button-error b_eliminar_peli'><i class='but_eliminar fa fa-times' aria-hidden='true'></i></button>";
      caratules += "<img src=" + url_caratula_p + " class='caratules'/>";
      caratules += "</div>";
    }

    document.getElementById("caratules_actuals").innerHTML = caratules;
  }

  // Borrar caratules //
  function borrar_caratula_bd(caratula_seleccionada) {

    caratula_seleccionada = caratula_seleccionada.substring(9);

    $.ajax({
      method: "DELETE",
      url: direccio_servidor + "/admin/api.php/records/caratules/" + caratula_seleccionada,
    })
      .done(function () {
        location.reload();
      });

  }

  function borrar_caratula_al_server(caratula_seleccionada, path_to_file) {
    
    path_to_caratula = "../asset/caratules/"+path_to_file;
    console.log(path_to_caratula);
    path_to_caratula = path_to_caratula.split('%20').join(' ');
    console.log(path_to_caratula);

    $.ajax({
      url: 'remover.php',
      data: { 'caratula': path_to_caratula },
      type: 'post',
      success: function () {
        borrar_caratula_bd(caratula_seleccionada);
      },
      error: function (error) {
        alert("Error al borrar l'arxiu " + error);
      }
    });
  }

  $.ajax({
    method: "GET",
    url: direccio_servidor + "/admin/api.php/records/caratules",
  })
  .done(function (res) {
    mostrar_caratules(res);
    // Borrar caratula //   
    $(".b_eliminar_peli").click(function () {
      caratula_seleccionada = $(this).parent().attr("id");
      path_to_file = $(this).parent().attr("attr");
    
      borrar_caratula_al_server(caratula_seleccionada, path_to_file);
        
    });

  });
});