
let api_key = "f82a26eeadaf6f0dce620f75375f93d2";

$(document).ready(function () {

    jQuery.ajaxSetup({
        beforeSend: function() {
           $('#loadingDiv').show();
        },
        complete: function(){
           $('#loadingDiv').hide();
        },
        success: function() {}
      });

    // Al refrescar pagina s'esborra les dades anteriors.
    document.getElementById("llistat_pelicules").innerHTML = "";
    
    //**************************** LLISTAT DE PELICULES ***********************************//

    // Busca les pelicules trending de la setmana.
    buscar_pelicules_trending();

    // Al fer click al buto de cerca
    $("#buscar_peli").click(function () {
        buscar_pelicules();
    });   

    function imprimir_llistat(res){
        // Amaga els trailers al buscar una altra pelicula.
        document.getElementById("video_pelicula").innerHTML = "";

        let html = "";

        // Si no es retorna cap pelicula es mostra missatge de error //
        if (res.results.length != 0) {

            for (np = 0; np < res.results.length; np++) {

                let release_date = res.results[np].release_date.split('-').reverse().join('/');

                // Si poster_path es null es mostra poster placeholder //
                if (res.results[np].poster_path) {
                    url = "https://image.tmdb.org/t/p/w500" + res.results[np].poster_path;
                } else {
                    url = "https://cdn.amctheatres.com/Media/Default/Images/noposter.jpg";
                }

                html += "<div class='pelicula' id='peli_" + res.results[np].id + "'>";
                html += "<img src=" + url + "  class='caratules'></img>";
                html += "<div class='info_pelicula'>";
                html += "<p class='titol_pelicula'>" + res.results[np].title + "</p>";
                html += "<p>" + release_date + "</p>";
                html += "</div>";
                html += "</div>";
            }
        } else {
            html += "<span> No s'ha trobat cap pel·lícula amb aquest titol. </span>";
        }

        document.getElementById("llistat_pelicules").innerHTML = html;
    }

    function buscar_pelicules_trending(){
        $.ajax({
            method: "GET",
            url: "https://api.themoviedb.org/3/trending/movie/week?api_key=f82a26eeadaf6f0dce620f75375f93d2",
            timeout: 3000,
        })
            .done(function (res) {
                imprimir_llistat(res);
            })
    }
    
    function buscar_pelicules(){
        let titol_a_buscar = document.getElementById("get_title_movie").value;

        $.ajax({
            method: "GET",
            url: "https://api.themoviedb.org/3/search/movie?api_key=" + api_key + "&query=" + titol_a_buscar + "&language=es",
            timeout: 3000,
        })
        .done(function (res) {
            imprimir_llistat(res);
        })
    }

    //**************************** PELICULA SELECCIONADA ***********************************//

    function mostra_pelicula_seleccionada(res){

        //console.log(res);

        $(".box_buscar_trailer").show();
        document.getElementById("video_pelicula").innerHTML = "";
        html = "";

        // Si poster_path es null es mostra poster placeholder //
        if (res.poster_path) {
            url = "https://image.tmdb.org/t/p/w500" + res.poster_path;
        } else {
            url = "https://cdn.amctheatres.com/Media/Default/Images/noposter.jpg";
        }

        // Informacio de la pelicula seleccionada //
        let id_pelicula_select = res.id;
        let titol_pelicula_select = res.title;
        let release_date_pelicula_select = res.release_date.split('-').reverse().join('/');
        let descripcio_pelicula_select = res.overview;

        html += "<div class='box_movie_select' id='peli_" + id_pelicula_select + "'>";
        html += "<div id='box_afegir_pelicula_db'><button class='button-success' id='afegir_pelicula_db'>Guardar</button></div>";
        html += "<img src=" + url + " class='caratules'></img>";
        html += "<div>"
        html += "<p class='titol_pelicula_select'>" + titol_pelicula_select + "</p> ";
        html += "<p class='desc_pelicula'>" + descripcio_pelicula_select + "</p>";
        html += "<p class='desc_pelicula'>" + release_date_pelicula_select + "</p>";
        html += '<div id="box_afegir_url_manual">';
        html += '<input type="text" id="get_url_movie" placeholder="Url de youtube de forma manual"/><br>';
        html += '</div>';
        html += "<div class='box_movie_select box_buscar_trailer'>";
        html += "<button class='button-secondary button_afegir_trailer'> Buscar Trailers <i class='fas fa-search'></i> </button>";
        html += '</div>';
        html += "</div>";
        html += "</div>";
        
        
        document.getElementById("llistat_pelicules").innerHTML = html;
    }

    function buscar_trailer_youtube(titol_pelicula_select) {
            
            $(".box_buscar_trailer").hide();
            quantitat_videos = 5;
            html_videos = "";

            gapi.client.setApiKey("AIzaSyAhkjY2yfu7K_W4ydyj1xWqyx1Fljiynvc");
            return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
            .then(function () {
                return gapi.client.youtube.search.list({
                    "part": "snippet",
                    "maxResults": quantitat_videos,
                    "q": "trailer " + titol_pelicula_select + " español"
            })
            .then(function (response) {
                html_videos+='<div id="box_trailer_selecionar">';
                for (n = 0; n < quantitat_videos; n++) {
                    url_cada_video = response.result.items[n].id.videoId;
                    
                    html_videos += '<div class="box_trailer">';
                    html_videos += '<iframe src="https://www.youtube.com/embed/' + url_cada_video + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
                    html_videos += '<div class="box_afegir_video">';
                    html_videos += '<button id="b_'+n+'" class="button_black button_afeigr_video" attr="'+url_cada_video+'"> Guardar Trailer</button>';
                    html_videos += '<div id="seleccionat_'+n+'"></div>';
                    html_videos += '</div>';
                    html_videos += '</div>';
                }
                html_videos+='</div>';

                document.getElementById("video_pelicula").innerHTML = html_videos;
            },
            function (err) { console.error("Execute error", err); });

            },
            function (err) { console.error("Error loading GAPI client for API", err); });
    }

    gapi.load("client");

    // Afegir check al video seleccionat //
    function video_seleccionat(buto_select){
        $("button").show();
        $(".box_seleccionat").removeClass("box_seleccionat");
        document.getElementsByClassName("box_seleccionat").innerHTML = "";
        
        $("#b_"+buto_select).hide();

        let id_seleccionat = "seleccionat_"+buto_select;
        $("#"+id_seleccionat).addClass("box_seleccionat");
        document.getElementById(id_seleccionat).innerHTML = "Guardat";
    }

    // Guardar Pelicula a la base de dades //
    function afegir_pelicula(id_pelicula_button,id_pelicula_select,titol_pelicula_select,descripcio_pelicula_select,url_poster,url_video,duracio,gen){   
        if (id_pelicula_select == id_pelicula_button){
            $.ajax({
                method: "POST",
                url: direccio_servidor+"/admin/api.php/records/pelicules",
                data:{
                    titol:titol_pelicula_select,
                    descripcio:descripcio_pelicula_select,
                    poster_path:url_poster,
                    url_video:url_video,
                    duracio:duracio,
                    estat:0,
                    generes:gen
                }
            })
            .done(function () {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    title: 'Pelicula afegida a la biblioteca',
                    showConfirmButton: false,
                    timer: 1500
                })
                console.log(titol_pelicula_select+" afegida");
            })
        }
    }

    $("#llistat_pelicules").on("click", ".pelicula", function () {

        // Recuperme l'id per fer la peticio d'aquesta pelicula //
        id_pelicula_selec = $(this).attr('id').substring(5);

        $.ajax({
            method: "GET",
            url: "https://api.themoviedb.org/3/movie/" + id_pelicula_selec + "?api_key=" + api_key + "&language=es",
            timeout: 3000,
        })
        .done(function (res) {

            console.log(res);

            let titol_pelicula_select = "";
            let descripcio_pelicula_select = "";
            let url_poster = "";
            let duracio = "";
            let url_pelicula_manula = "";
            let url_video = "";
            titol_pelicula_select = res.title;

            let generes = "";
            for(cont=0; cont<res.genres.length;cont++){
                generes += '#'+res.genres[cont].name;
            }

            console.log(generes);

            mostra_pelicula_seleccionada(res);
            buscar_trailer_youtube(titol_pelicula_select);
                
            $("#video_pelicula").on("click", ".button_afeigr_video", function () {
                url_video= $(this).attr('attr');
                buto_select = $(this).attr('id').substring(2);
                video_seleccionat(buto_select);
            });

            $("#llistat_pelicules").on("click", "#afegir_pelicula_db", function () {
                id_pelicula_button = $(this).parent().parent().attr('id').substring(5);

                id_pelicula_select = res.id;
                titol_pelicula_select = res.title;
                descripcio_pelicula_select = res.overview;
                url_poster = "https://image.tmdb.org/t/p/w500" + res.poster_path;
                duracio = res.runtime;

                url_pelicula_manula = $("#get_url_movie").val().replace("https://www.youtube.com/watch?v=", "");

                if(url_pelicula_manula != ""){
                    url_video = url_pelicula_manula;
                }

                if ((url_video == "") && (url_pelicula_manula == "")){
                    Swal.fire({
                        position: 'center',
                        type: 'info',
                        title: 'Selecciona un tràiler',
                        showConfirmButton: false,
                        timer: 1500
                    })
                }else{
                    afegir_pelicula(id_pelicula_button,id_pelicula_select,titol_pelicula_select,descripcio_pelicula_select,url_poster,url_video,duracio,generes);

                    titol_pelicula_select = "";
                    descripcio_pelicula_select = "";
                    url_poster = "";
                    duracio = "";
                    url_video = "";
                    url_pelicula_manula = "";
                }



            });

        })
    });
});
