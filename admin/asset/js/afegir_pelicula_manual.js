$(document).ready(function () {

    function guardar_poster_al_server(form_data) {
        $.ajax({
            url: 'uploader_poster.php', 
            dataType: 'text', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function () {
                console.log("S'ha pujar l'arxiu");
            },
            error: function () {
              console.log("error al pujar l'arxiu");
            }
          });
    }

    function pujar_pelicula_m_bd(p_pelicula_m,t_pelicula_m,d_pelicula_m,durada_pelicula_m,v_pelicula_m){
        
        $.ajax({
            method: "POST",
            url: direccio_servidor+"/admin/api.php/records/pelicules",
            data:{
                titol:t_pelicula_m,
                descripcio:d_pelicula_m,
                poster_path:p_pelicula_m = p_pelicula_m.split(' ').join('%20'),
                url_video:v_pelicula_m,
                duracio:durada_pelicula_m,
                estat:0,
                generes:""
            }
        })
        .done(function () {
            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Pelicula afegida a la biblioteca',
                showConfirmButton: false,
                timer: 1500
              })
        })
    }

    // Guardar a la bd la caratula i pujar al servidor
    $('#afegir_manual').on('click', function () {

        let file_data = $('#p_pelicula').prop('files')[0];
        let form_data = new FormData();
        form_data.append('file', file_data);

        console.log(file_data);

        if (typeof file_data === 'undefined'){
            Swal.fire({
                position: 'center',
                type: 'info',
                title: 'Selecciona un poster',
                showConfirmButton: false,
                timer: 800
              })
        }else{
            p_pelicula_m = "../asset/img/posters/"+file_data.name;
            t_pelicula_m = $("#t_pelicula_m").val();
            d_pelicula_m = $("#d_pelicula_m").val();
            durada_pelicula_m = $("#durada_pelicula_m").val();
            v_pelicula_m = $("#v_pelicula_m").val().replace("https://www.youtube.com/watch?v=", "");

            pujar_pelicula_m_bd(p_pelicula_m,t_pelicula_m,d_pelicula_m,durada_pelicula_m,v_pelicula_m);
            guardar_poster_al_server(form_data);
        }

    });
});