<?php
session_start();
if (isset($_SESSION['master'])){
?>
<!DOCTYPE html>
<html lang="cat">
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" /> 

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="asset/css/reset.css">
        <link rel="stylesheet" type="text/css" href="asset/css/admin.css">
        <link rel="stylesheet" type="text/css" href="asset/css/menu.css">
        <link rel="stylesheet" type="text/css" href="asset/css/a_manual.css">

        <!-- Jquery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

        <!-- Sweet Alert2 -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        
        <title> Afegir Pelicula Manualment </title>

    </head>
    <body>
        <script src="server.js"></script>
        <div id="menu_navegacio"></div>
        <script src="asset/js/nav.js"></script>

        <div class="box_cos">
            <script src="asset/js/afegir_pelicula_manual.js"></script>
    
            <div id="box_form_a_pelicula_m">
                <input class="button_pujar_img" id="p_pelicula" type="file" name="poster_pelicula" />
                <input type="text" id="t_pelicula_m" name="titol_pelicula" placeholder="Títol" autofocus/>
                <input type="text" id="d_pelicula_m" name="descripcio_pelicula" placeholder="Descripció"/>
                <input type="text" id="durada_pelicula_m" name="duracio_pelicula" placeholder="Duració"/>
                <input type="text" id="v_pelicula_m" name="video_pelicula" placeholder="URL Video"/>
                <br>
                <button id="afegir_manual" class="button-success">Afegir</button>
            </div>

        </div>
    </body>
</html>
<?php
}else{
	header("location: ../admin/in_sessio.php");	
}
?>