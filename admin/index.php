<?php
session_start();
if (isset($_SESSION['master'])){
?>
<!DOCTYPE html>
<html lang="cat">
    <head>
        <meta charset="UTF-8">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" /> 

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="asset/css/reset.css">
        <link rel="stylesheet" type="text/css" href="asset/css/biblioteca.css">
        <link rel="stylesheet" type="text/css" href="asset/css/admin.css">
        <link rel="stylesheet" type="text/css" href="asset/css/menu.css">

        <!-- Jquery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

        <!-- Sweet Alert2 -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        
        <title> Administració Cine París </title>

    </head>
    <body>
        <script src="server.js"></script>

        <?php
            include 'asset/funcions/api.php';
        ?>
        
        <div id="menu_navegacio">
            <script src="asset/js/nav.js"></script>
        </div>
        
        <div class="box_cos">

            <div class="button_desplegable"> 
                <p>Caratules Principals</p>
            </div>
            <div class ="box_despelgable">
                <div class="button_pujar_img">
                    <input id="sortpicture" type="file" class="inputfile" name="sortpic"/>
                    <label class="button_pujar_caratula" id="seleccionar_file" for="sortpicture">
                        <i class="fas fa-upload"></i>
                        <span>Seleccionar caràtula</span>
                    </label>
                    <button class="button_pujar_caratula" id="pujar_file">
                        <i class="fas fa-upload"></i>
                        <span>Pujar caràtula</span>
                    </button>
                </div>

                <div id="caratules_actuals"></div>
            </div>

            <div class="button_desplegable"> 
                <p>Biblioteca</p>
            </div>
            <div id='biblioteca_pelicules'></div>
        </div>
        <script src="asset/js/caratula_principal.js"></script>
        <script src="asset/js/biblioteca.js"></script>
    </body>
</html>
<?php
}else{
	header("location: ../admin/in_sessio.php");	
}
?>