<?php
session_start();
if (isset($_SESSION['master'])){
?>
<!DOCTYPE html>
<html lang="cat">
  <head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" /> 

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="asset/css/reset.css">
    <link rel="stylesheet" type="text/css" href="asset/css/admin.css">
    <link rel="stylesheet" type="text/css" href="asset/css/menu.css">

    <!-- Jquery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>

    <!-- Sweet Alert2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <!-- API YOUTUBE -->
    <script src="https://apis.google.com/js/api.js"></script>

    <title> Afegir Pel·lícula </title>
  </head>
  <body>
    <script src="server.js"></script>

    <?php
      include 'asset/funcions/api.php';
    ?>

    <div id="menu_navegacio"></div>
    <script src="asset/js/nav.js"></script>

    <div class="box_cos">

      <div id="box_cerca_peli">
          <input type="text" id="get_title_movie" placeholder="Titol pel·lícula"/>
          <button class="button_black" id="buscar_peli"><i class="fas fa-search"></i></button>
      </div>

      <!--loading-->
      <div id="loadingDiv">
        <div class="sk-folding-cube">
          <div class="sk-cube1 sk-cube"></div>
          <div class="sk-cube2 sk-cube"></div>
          <div class="sk-cube4 sk-cube"></div>
          <div class="sk-cube3 sk-cube"></div>
        </div>
      </div>

      <div id='llistat_pelicules'></div>
      <script src="asset/js/afegir_pelicula.js"></script>
          <div id ="video_pelicula"></div>
    </div>
  </body>
</html>
<?php
}else{
	header("location: ../admin/in_sessio.php");	
}
?>