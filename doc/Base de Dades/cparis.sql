-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 24-05-2019 a les 11:27:05
-- Versió del servidor: 5.7.26-0ubuntu0.16.04.1
-- Versió de PHP: 7.0.33-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `cparis`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `caratules`
--

CREATE TABLE `caratules` (
  `id` int(11) NOT NULL,
  `url` varchar(200) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Bolcant dades de la taula `caratules`
--

INSERT INTO `caratules` (`id`, `url`) VALUES
(89, 'IMG_20190520_091703.jpg');

-- --------------------------------------------------------

--
-- Estructura de la taula `pelicules`
--

CREATE TABLE `pelicules` (
  `id` int(11) NOT NULL,
  `titol` varchar(500) COLLATE latin1_spanish_ci NOT NULL,
  `descripcio` text COLLATE latin1_spanish_ci NOT NULL,
  `poster_path` varchar(300) COLLATE latin1_spanish_ci NOT NULL,
  `url_video` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `duracio` varchar(11) COLLATE latin1_spanish_ci NOT NULL,
  `estat` int(11) NOT NULL,
  `generes` varchar(300) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Bolcant dades de la taula `pelicules`
--

INSERT INTO `pelicules` (`id`, `titol`, `descripcio`, `poster_path`, `url_video`, `duracio`, `estat`, `generes`) VALUES
(236, 'Pokémon Detective Pikachu', 'Ryme City, una metrópoli futurista en la que los humanos y los Pokémon conviven en armonía. Tras la misteriosa desaparición de su padre, toda una leyenda en la ciudad, el joven Tim Goodman (Justice Smith) comienza una investigación para buscarle y averiguar lo que le ha sucedido. En esta misión le ayudará el adorable súper-detective Pikachu, un inteligente Pokémon que habla, aunque curiosamente el chico es el único que puede entenderle. Ambos unirán sus fuerzas y trabajarán juntos para resolver este gran misterio, con la ayuda de Lucy (Kathryn Newton), una reportera que trabaja en su primera gran historia. Será una aventura en la que descubrirán a entrañables personajes del universo Pokémon, además de una trama espeluznante que podría amenazar la convivencia pacífica de todo este universo.', 'https://image.tmdb.org/t/p/w500/dvuaZb2Wv5ptFS33bh1OWkKY1zx.jpg', 'XzCj0lGfveU', '105', 2, '#Misterio#Familia#Comedia#Ciencia ficción#Acción#Aventura#Animación'),
(237, 'De la India a París en un armario de Ikea', 'Aja, un joven estafador de Mumbai, comienza, tras la muerte de su madre, un viaje extraordinario siguiendo las huellas del padre que nunca conoció. Encuentra el amor en París en una tienda de muebles suecos, el peligro en compañía de inmigrantes somalíes en Inglaterra, la fama en una pista de baile en Roma, la aventura en un globo aerostático sobre el Mediterráneo, y finalmente entiende qué es la verdadera riqueza y en quién quiere convertirse.', 'https://image.tmdb.org/t/p/w500/lVroIWDXLPTqS29wlnL10mbQ2K5.jpg', 'jtNUIYo2MvM', '92', 1, '#Comedia'),
(238, 'Feliz día de tu muerte 2', 'Dos años después de los eventos acaecidos en la primera película, Tree Gelbman vuelve a entrar en el bucle temporal para descubrir el motivo por el cual accedió a él en primer lugar. También debe hacer frente a Lori, que tras resucitar a causa del buble ha vuelto sedienta de venganza. Secuela de "Happy Death Day" (2017).', 'https://image.tmdb.org/t/p/w500/q6PSTBNlI9WtfuMlngz82yaudUg.jpg', 'eZicXnGtdWU', '100', 1, '#Terror#Misterio#Suspense#Ciencia ficción#Comedia'),
(239, 'La LEGO película 2', 'Han pasado cinco años desde que todo era fabuloso y los ciudadanos se enfrentan a una gran nueva amenaza: los invasores LEGO DUPLO® del espacio exterior, destruyendo todo más rápido de lo que pueden reconstruir.', 'https://image.tmdb.org/t/p/w500/4L084cmvuAROSEaQjDEpernXNEn.jpg', 'WPiQJyTwOPY', '107', 1, '#Acción#Aventura#Animación#Comedia#Familia#Ciencia ficción#Fantasía'),
(240, 'John Wick 3: Parabellum', 'John Wick (Keanu Reeves) regresa a la acción, solo que esta vez con una recompensa de 14 millones de dólares sobre su cabeza y con un ejército de mercenarios intentando darle caza. Tras asesinar a uno de los miembros del gremio de asesinos al que pertenecía, Wick es expulsado de la organización, pasando a convertirse en el centro de atención de multitud de asesinos a sueldo que esperan detrás de cada esquina para tratar de deshacerse de él.', 'https://image.tmdb.org/t/p/w500/38d32uG1x7iiN2jdK0cRX0Bk423.jpg', 'T458DJSXhUo', '131', 1, '#Crimen#Acción#Suspense'),
(241, 'La Llorona', 'La Llorona es una aparición tenebrosa, atrapada entre el Cielo y el Infierno, con un destino terrible sellado por su propia mano. La mera mención de su nombre ha causado terror en todo el mundo durante generaciones. En vida, ahogó a sus hijos en una rabia de celos, arrojándose en el río tras ver como mató a sus hijos violentamente. Ahora sus lágrimas son eternas, letales, y aquellos que escuchan su llamada de muerte en la noche están condenados. Se arrastra en las sombras y ataca a los niños, desesperada por reemplazar a los suyos. A medida que los siglos han pasado, su deseo se ha vuelto más voraz y sus métodos más terroríficos.', 'https://image.tmdb.org/t/p/w500/1BIWytsnEr2aRobU6ERutzewm63.jpg', 'sx1p1AIHjY4', '93', 1, '#Suspense#Terror#Misterio'),
(242, 'Mia y el león blanco', 'Una joven que se ha trasladado con sus padres desde Londres a África desarrolla un vínculo sorprendente y especial con un león salvaje. Su increíble amistad la impulsa a viajar por la sabana para salvar a su mejor amigo.', 'https://image.tmdb.org/t/p/w500/jU4BG0zcp3tNhfC1qUecaHezie4.jpg', 'JZsQNX8xNv0', '98', 1, '#Aventura#Drama#Familia'),
(243, 'Bumblebee', 'Sexta entrega de la saga \'Transformers\', esta vez centrada en el "hermano pequeño" de los Autobots, Bumblebee. Tratando de escapar, en el año 1987, Bumblebee encuentra refugio en un depósito de chatarra en una pequeña ciudad en la costa Californiana. Charlie (Hailee Steinfeld), a punto de cumplir 18 años y tratando de encontrar su lugar en el mundo, descubre a Bumblebee, dañado durante una batalla y descompuesto. Cuando Charlie lo revive, aprende rápidamente que éste no es un VW amarillo ordinario.', 'https://image.tmdb.org/t/p/w500/lOW8vBMJqWHWcMxHbigVAytqiBn.jpg', 'pucggiepP-0', '114', 0, '#Acción#Aventura#Ciencia ficción');

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `caratules`
--
ALTER TABLE `caratules`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `pelicules`
--
ALTER TABLE `pelicules`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `caratules`
--
ALTER TABLE `caratules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT per la taula `pelicules`
--
ALTER TABLE `pelicules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
