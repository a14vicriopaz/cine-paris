# WEB CINE PARÍS (SOLSONA) #

### Integrants del grup: ###

* Carles Vila
* Victor Rios

### Descripció: ###

Actualment la web del Cine París està realitzada amb Wordpress amb una plantilla bàsica. Per pujar les pel·lícules es modifica la pàgina principal canviant les dades, el disseny d’aquesta pàgina es va implementar el 2006.
Es vol canviar de wordpress a un model fet amb html5, mysql, javascript i php a més utilitzant apis externes per poder aconseguir les dades de les pel·lícules.

### Guia de posada en marxa ###

* Pujar les carpetes admin i asset, tambe el fitxer index.html al vostre servidor.
* Crear una nova base de dades al vostre servidor.
* Importar el sql que es troba dins de doc/Base de Dades.
* Modificar l'arxiu que es troba a admin/server.js i indicar la ip o nom de domini del vostre servidor.
* Modificar l'arxiu que es troba a admin/api.php, al final del arxiu, podeu veure que s'ha d'indicar quines credencials i a on es te que conectar per poder accedir a la base de Dades.

* Seguint aquests passos ja auria de funcionar tot.

* Si es vol cambiar les credencials es pot modificar l'arxiu admin/in_sessio.php, aqui podras trobar 2 variables $mu => usuari, $mpasswd = contrasenya.